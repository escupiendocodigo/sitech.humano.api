# SITECH.Humano.API



## Introducción

Desarrollo de backend en .NET CORE 3 el cual contiene las siguientes características

API tipo GET que retorne un arreglo de objetos de tipo "HUMANO",esta información será mock
```
/api/Humano/Mocks
```

•API tipo POST que se reciba 3 argumentos para la realización de una operación matemática y retorne el resultado
```
NOTA: Para el parametro de TipoOperacion Verificar la sección Nota
/api/Humano/OperacionMatematica
```
•API tipo GET que reciba 3 argumentos en Headers argumentos para la realización de una operación matemática y retorne el resultado
```
Headers debe contener los siguientes parametros para su optimo funcionamiento: Numero1,Numero2,TipoOperacion
NOTA: Para el parametro de TipoOperacion Verificar la sección Nota
/api/Humano/OperacionMatematica
```
•Creaciónde migración para una base de datoscon tabla humanos

## Comandos Entity
```
Add-Migration Inicial: Generará una clase de tipo migración para poder montala dentro de un motor de base de datos SQL.

Update-Database: Ejecuta la ultima configuración del comando anterior
```

•API tipo GET para traer toda la tabla de humanos no es necesario especificar el id
```
/api/Humano/Obtener
```

•API tipo GET para traer un humano en especifico se tiene que especificar el id
```
/api/Humano/Obtener
```

•API tipo POST para crear un humano
```
/api/Humano/Crear
```

•API tipo PUT para editar un humano
```
/api/Humano/editar
```

## NOTA:
```
Para consultar los metodos de "OperacioneMatematica" el tipo de Operación seria del tipo básicas:
suma = 1,
resta = 2,
multiplicación = 3,
división = 4
el valor en entero es lo que se debe enviar al servicio para un buen comportamiento.
```

## Comandos para Clonar el repositorio

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/escupiendocodigo/sitech.humano.api.git
git branch -M main
git push -uf origin main
```

## License
For open source projects, say how it is licensed.

## Como usarlo 
Solo descarga o clona el repositorio y ejectutalo con F5
