﻿using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Protocols;

namespace _SITECH.Humano.API.db
{
    public class HumanoContext : DbContext
    {
        public HumanoContext(string connectionString) : base(GetOptions(connectionString))
        {
        }

        private static DbContextOptions GetOptions(string connectionString)
        {
            return SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), connectionString).Options;
        }
        public HumanoContext(DbContextOptions<HumanoContext> options) :
            base(options)
        {

        }

        public DbSet<Models.Humano> Humano { get; set; }

       
    }
}
