﻿namespace _SITECH.Humano.API.Enums
{
    public enum EnumTipoOperacion
    {
        Suma = 1,
        Resta = 2,
        Multiplicacion = 3,
        Division = 4
    }
}
