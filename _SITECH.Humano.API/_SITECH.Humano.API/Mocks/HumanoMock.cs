﻿using System;
using System.Collections.Generic;

namespace _SITECH.Humano.API.Mocks
{
    public class HumanoMock
    {
        public static readonly string[] Nombres = new[]
        {
            "Kuroko", "Goku", "Monkey D.Luffy", "Kirito", "Azuma", "Smile", "Leo", "Vegeta", "Milk", "Bulma"
        };
        public static readonly string[] Sexo = new[]
        {
            "Masculino", "Femenino", "Indistinto"
        };

        public static readonly int[] IdyEdad = new[]
        {
            10,20,30,40,50,60,70,80,90,100
        };

        public static readonly double[] Altura = new[]
        {
            1.10,2.0,1.30,1.40,1.50,1.60,1.70,1.80,1.90,1.100
        };
        public static readonly double[] Peso = new[]
        {
            10.10,20.20,30.30,40.40,50.50,60.60,70.70,80.80,90.90,100
        };
    }

    public interface IEnumerableClass : IEnumerable<Models.Humano>
    {
    }
}
