﻿using _SITECH.Humano.API.Enums;
using _SITECH.Humano.API.ViewModels;
using System;
using System.Collections.Generic;

namespace _SITECH.Humano.API.Services
{
    public interface IHumanoService
    {
        List<Models.Humano> MockHumanoAlls();
        Tuple<string, decimal> MockOperacionMatematica(OperacionMatematicaRequest operacionMatematicaRequest);
    }
}
