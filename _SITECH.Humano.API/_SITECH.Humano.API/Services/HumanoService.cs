﻿using _SITECH.Humano.API.Enums;
using _SITECH.Humano.API.Mocks;
using _SITECH.Humano.API.ViewModels;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace _SITECH.Humano.API.Services
{
    public class HumanoService : IHumanoService
    {


        public List<Models.Humano> MockHumanoAlls()
        {
            var humanosLista = new List<Models.Humano>();
            try
            {
                var rng = new Random();
                for (int i = 0; i < 10; i++)
                {
                    humanosLista.Add(new Models.Humano()
                    {
                        Id = HumanoMock.IdyEdad[rng.Next(HumanoMock.IdyEdad.Length)],
                        Nombre = HumanoMock.Nombres[rng.Next(HumanoMock.Nombres.Length)],
                        Sexo = HumanoMock.Sexo[rng.Next(HumanoMock.Sexo.Length)],
                        Altura = (decimal)HumanoMock.Altura[rng.Next(HumanoMock.Altura.Length)],
                        Peso = (decimal)HumanoMock.Peso[rng.Next(HumanoMock.Peso.Length)],
                        Edad = HumanoMock.IdyEdad[rng.Next(HumanoMock.IdyEdad.Length)],
                    });
            }

                var mock = new Mock<IEnumerableClass>();
                mock.Setup(x => x.GetEnumerator()).Returns(humanosLista.GetEnumerator());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return humanosLista;
        }

      
        public Tuple<string, decimal> MockOperacionMatematica(OperacionMatematicaRequest operacionMatematicaRequest)
        {
            try
            {
                if (operacionMatematicaRequest.Numero1 <= 0)
                {
                    throw new Exception("NUMERO 1 DEBE SER MAYOR A CERO O NO EXISTE DENTRO DE LA PETICIÓN");
                }
                if (operacionMatematicaRequest.Numero2 <= 0)
                {
                    throw new Exception("NUMERO 2 DEBE SER MAYOR A CERO O NO EXISTE DENTRO DE LA PETICIÓN");
                }
                decimal num1 = operacionMatematicaRequest.Numero1;
                decimal num2 = operacionMatematicaRequest.Numero2;
                int tipoOperacion = operacionMatematicaRequest.TipoOperacion;

                switch (tipoOperacion)
                {
                    case (int)EnumTipoOperacion.Suma:
                        return new Tuple<string, decimal>(nameof(EnumTipoOperacion.Suma).ToUpper(), (num1 + num2));
                    case (int)EnumTipoOperacion.Resta:
                        return new Tuple<string, decimal>(nameof(EnumTipoOperacion.Resta).ToUpper(), (num1 - num2));
                    case (int)EnumTipoOperacion.Multiplicacion:
                        return new Tuple<string, decimal>(nameof(EnumTipoOperacion.Multiplicacion).ToUpper(), (num1 * num2));
                    case (int)EnumTipoOperacion.Division:
                        return new Tuple<string, decimal>(nameof(EnumTipoOperacion.Division).ToUpper(), (num1 - num2));
                    default:
                        throw new Exception("TIPO DE OPERACION NO ES UNA OPCIÓN VALIDA");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

