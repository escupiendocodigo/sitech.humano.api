using _SITECH.Humano.API.db;
using _SITECH.Humano.API.Enums;
using _SITECH.Humano.API.Services;
using _SITECH.Humano.API.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace _SITECH.Humano.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HumanoController : ControllerBase
    {
        public IHumanoService humanoService{ get; set; }
        public HumanoContext _context  { get; set; }
        public HumanoController(IHumanoService _humanoService, HumanoContext context)
        {
            humanoService = _humanoService;
            _context = context;
        }

        #region [ ENDPOINT's DINAMICOS]
        /// <summary>
        /// API tipo GET que retorne un arreglo de objetos de tipo “HUMANO”, esta información será mock
        /// </summary>
        /// <returns></returns>
        [HttpGet("Mocks")]
        public IActionResult Todos()
        {
            try
            {
                var response = humanoService.MockHumanoAlls();
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// API tipo POST que se envíen 3 argumentos para la realización de una operación matemática y retorne el resultado
        /// </summary>
        /// <param name="num1"></param>
        /// <param name="num2"></param>
        /// <param name="tipoOperacion"></param>
        /// <returns></returns>
        [HttpPost("OperacionMatematica")]
        public IActionResult OperacionMatematica(OperacionMatematicaRequest operacionMatematicaRequest)
        {
            try
            {
                var respuesta = humanoService.MockOperacionMatematica(operacionMatematicaRequest);
                return Ok($"EL RESULTADO DE {respuesta.Item1} ES: {respuesta.Item2}");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private static void LeerValorHeader(out string key, HttpContext context)
        {
            key = string.Empty;
            if (context.Request.Headers.TryGetValue("num1", out var traceValue))
            {
                key = traceValue;
            }
        }

        /// <summary>
        /// API tipo GET que reciba 3 argumentos en Headers argumentos para la realización de una operación matemática y retorne el resultado
        /// </summary>
        /// <returns></returns>
        [HttpGet("OperacionMatematica")]
        public IActionResult OperacionMatematica()
        {
            try
            {
                decimal num1 = 0;
                decimal num2 = 0;
                int tipoOperacion=0;

                var operacionMatematicaRequest = new OperacionMatematicaRequest();

                Request.Headers.TryGetValue(nameof(operacionMatematicaRequest.Numero1), out var num1Value);
                decimal.TryParse(num1Value, out num1);

                Request.Headers.TryGetValue(nameof(operacionMatematicaRequest.Numero2), out var num2Value);
                decimal.TryParse(num2Value, out num2);

                Request.Headers.TryGetValue(nameof(operacionMatematicaRequest.TipoOperacion), out var enumTipoOperacionValue);
                int.TryParse(enumTipoOperacionValue, out tipoOperacion);

                
                operacionMatematicaRequest.Numero1 = num1;
                operacionMatematicaRequest.Numero2 = num2;
                operacionMatematicaRequest.TipoOperacion = tipoOperacion;

                var respuesta = humanoService.MockOperacionMatematica(operacionMatematicaRequest);

                return Ok($"EL RESULTADO DE {respuesta.Item1} ES: {respuesta.Item2}");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region [MÉTODOS DE CLASE PARA DEFINIR UNA BASE DE DATOS]
        /// <summary>
        /// API tipo GET para traer toda la tabla de humanos
        /// API tipo GET para traer un humano en especifico
        /// </summary>
        /// <param name="id">Si es nullo regresará todos los registros existentes y si contiene un id regresara unicamente el id solicitado</param>
        /// <returns></returns>
        [HttpGet("Obtener")]
        public IActionResult HumanosList(int? id)
        {
            try
            {
                var lista = _context.Humano.Where(s => (id.HasValue) ? s.Id == id : s.Id == s.Id).ToList();
                return Ok(lista);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// API tipo POST para crear un humano
        /// </summary>
        /// <param name="humanoRequest"></param>
        /// <returns></returns>
        [HttpPost("Crear")]
        public IActionResult CrearHumano(HumanoRequest humanoRequest)
        {
            try
            {
                var model = new Models.Humano()
                {
                    Nombre= humanoRequest.nombre,
                    Sexo = humanoRequest.sexo,
                    Edad = humanoRequest.edad,
                    Altura = humanoRequest.altura,
                    Peso = humanoRequest.peso
                };
                _context.Humano.Add(model);

                _context.SaveChanges();
                return Ok(humanoRequest);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// API tipo PUT para editar un humano
        /// </summary>
        /// <param name="humanoRequest"></param>
        /// <returns></returns>
        [HttpPut("editar")]
        public IActionResult ModificarProceso(HumanoRequest humanoRequest)
        {
            try
            {
                var model = _context.Humano.Where(s => s.Id == humanoRequest.id).FirstOrDefault();

                if (model == null)
                {
                    throw new Exception("HUMANO NO EXISTE, VERIFICAR SU CONSULTA");
                }
                else
                {
                    if (humanoRequest.id != model.Id)
                    {
                        throw new Exception("HUMANO NO COINCIDE CON LA INFORMACIÓN GUARDADA");
                    }
                    else
                    {
                        model.Nombre = humanoRequest.nombre;
                        model.Sexo = humanoRequest.sexo;
                        model.Edad = humanoRequest.edad;
                        model.Altura = humanoRequest.altura;
                        model.Peso = humanoRequest.peso;
                    }
                    var modelUpdate = _context.Humano.Update(model);
                    _context.SaveChanges();
                    return Ok(modelUpdate.Entity);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #endregion

       
        
    }
}
