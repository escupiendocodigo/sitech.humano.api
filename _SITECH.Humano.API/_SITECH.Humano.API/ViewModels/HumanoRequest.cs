﻿namespace _SITECH.Humano.API.ViewModels
{
    public class HumanoRequest
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public int edad { get; set; }
        public string sexo { get; set; }
        public decimal altura { get; set; }
        public decimal peso { get; set; }
    }
}
