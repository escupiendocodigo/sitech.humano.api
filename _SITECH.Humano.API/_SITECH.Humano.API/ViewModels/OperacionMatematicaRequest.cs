﻿using _SITECH.Humano.API.Enums;

namespace _SITECH.Humano.API.ViewModels
{
    public class OperacionMatematicaRequest
    {
        public decimal Numero1 { get; set; }
        public decimal Numero2 { get; set; }
        public int TipoOperacion{ get; set; }
    }
}
